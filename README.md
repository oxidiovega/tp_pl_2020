# Sentiment Analaysis using IMDB reviews
this notebook contains a sentiment analysis workflow designed to classify reviews using the IMDB dataset
## requirements to run the notebook:

-Dataset [https://drive.google.com/file/d/1RE_eRJ2Ey8ttwHTXPPwUWxTUSppyRvCU/view?ts=602cc93d&fbclid=IwAR00ZMaSUQTfCwWnTPdhtJtNi9F97UIHYXiWQhbWXN6CoDp2CHkbkVoPMIs](url)

**online**
- Google colab
- IMDB dataset on your google drive 

ps : if you want to run this notebook **locally** just download the dataset at  and comment out / remove the second cell

**locally**
- nltk
- pandas
- scikit-learn
- matplotlib
- wordcloud

`pip install nltk pandas scikit-learn matplotlib wordcloud`










